# 概述
这个脚本主要是以git钩子的形式，来完成一个特定的功能：检测一个unreal project下的插件是否有改动，若有，则更新这些（改动了的）插件的uplugin文件里的版本号字段。  
目的就是追踪插件的版本号，这样从git代码库里任意一个commit签出一个版本给别人使用时，插件都是带有版本号的，使得我们能够追踪插件的出处。  
这个追踪的原理，见https://my.oschina.net/zhoubaojing/blog/4321081<BR>

**2022.09.20更新：** 出于对C#工程中追踪版本号的需要，实现了一个通用的追踪版本号的功能，其使用方法在【使用】这一节会详细描述。

# 细节
插件出现了改动，是指插件目录（即uplugin文件所在的目录）及其子目录下有提交到staging区域的改动。目前识别了Add、Delete、Rename、Copy、Modify类型的改动。

## 版本号生成的规则
如果是一个刚创建的repository，尚未发生过提交，则版本号为**当前日期#0000000**，如Dec-09-2021#0000000。  
如果repository里已有过commit，则新的版本号为**当前日期#最新的commit的HEXSHA前7位**，如Dec-09-2021#fd71103

注意[ff8442b](https://gitee.com/zbjxb/git-action-hooks/commit/ff8442bc39982d81c9884e66cf063ab81456964e)的修改，版本号规则修改为在上面描述的基础上追加6位十六进制随机数。原因在commit message里已解释。

install-pre-commit.bat用来安装hooks：就是简单的将pre-commit和pre-commit.py两个文件拷贝到.git/hooks/下。  
pre-commit其实是一个bat文件，也可以认为是shell文件，里面就一句用python执行pre-commit.py。这个文件就是hooks，利用git程序commit代码时，这个pre-commit会在commit前执行。  
pre-commit.py是真正的hooks逻辑。

# 使用
分两种使用场景来介绍：针对虚幻引擎的插件版本号追踪，以及针对其他通常的项目版本号追踪（简称为“其他情形”）。<BR>
**无论哪种情形，首先需要将包含common与ue-plugin-version文件夹的git-action-hooks文件夹拷贝到git库的根目录下，即与.git文件夹保持平级。**

## 虚幻引擎插件版本号追踪
### 安装钩子
执行一下git-action-hooks/ue-plugin-version/install-pre-commit.bat即可。
~~如果你定制了这个py脚本，记得再次运行install-pre-commit.bat重新安装钩子。~~ (已由[edc3e4a](https://gitee.com/zbjxb/git-action-hooks/commit/edc3e4a154afb154e12b170af8caf4ca6f7a832a)解决)


需要注意的地方还有，将UE4插件发给别人使用时，一定要严格遵守流程：只能从代码库里checkout某个commit版本发布出去，最好将要发布的版本打个tag，发布的东西一定都是某个tag版本，这样整个版本追踪的逻辑就工作的很顺利。不要随随便便将本地当前编译好的版本给出去，若如此便脱离了代码追踪的流程。见【概述】里描述的版本追踪原理。

以上用TortoiseGit测试功能正常。UE4编辑器工具栏Git选项提交时无法将uplugin文件写入，原因是UE4编辑器的git提交时，在git系统调用pre-commit钩子前，已经用index.lock锁定了staged area，我无法成功的将修改后的uplugin文件加入到staged area。


## 其他情形版本号追踪
### 安装钩子
执行一下git-action-hooks/common/install-pre-commit.bat即可。
### 创建配置文件
仿照git-action-hooks/common/config.template.json模板文件创建一个git-action-hooks/common/config.json文件。
```json
{
	"version_files": [
		{
			"template": "git-action-hooks/common/C/version_file.h.template", // relative to [.git/../]
			"placeholder": "##VERSION##",
			"output": "git-action-hooks/common/C/version_file.h" // relative to [.git/../]
		},
		{
			"template": "git-action-hooks/common/C#/version_file.cs.template",
			"placeholder": "##VERSION##",
			"output": "git-action-hooks/common/C#/version_file.cs"
		}
	]
}
```
`version_files`字段的值是一个json数组，该数组每个元素都是一个创建版本文件的动作。**通常只需持有一个动作即可**，即该json数组只保留一个元素。<BR>
当然，如果你确实需要同时产生多个版本号文件，则如json模板里所示，添加多个动作即可。

#### 动作
```json
{
    "template": "git-action-hooks/common/C#/version_file.cs.template",
    "placeholder": "##VERSION##",
    "output": "git-action-hooks/common/C#/version_file.cs"
}
```
动作是用一个json object描述的。包含3个字段：`template`、`placeholder`、`output`。<BR>
`template`：指定一个版本号文件的模板，参照version_file.cs.template，这个模板可以视自己的实际情况编写。`template`指定的模板文件必须是相对于git库根目录的路径。<BR>
`placeholder`：指定**版本号占位符**。占位符**必须**出现在`template`指定的模板文件中，动作执行中会将版本号占位符替换为版本号。<BR>
`output`：指定输出版本号文件的路径。`output`指定的版本号文件必须是相对于git库根目录的路径。<BR>

动作的执行过程是：读取模板文件，将模板文件中的版本号占位符替换为正确的版本号，将替换后的完整内容输出为指定的版本号文件。


# 注意
- ~~[a717110](https://gitee.com/zbjxb/git-action-hooks/commit/a717110491ca2a2b5c5090133f97302f3fc396be)~~ 已由[ff8442b](https://gitee.com/zbjxb/git-action-hooks/commit/ff8442bc39982d81c9884e66cf063ab81456964e)解决
- 如果git提交时总是提示错误，检查一下电脑是否已安装python。装上python后，运行一下代码库里的install-pymodules-depends.bat批处理
